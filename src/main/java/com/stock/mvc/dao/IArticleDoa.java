package com.stock.mvc.dao;

import com.stock.mvc.entities.Article;

public interface IArticleDoa extends IGenericDao<Article> {

}
