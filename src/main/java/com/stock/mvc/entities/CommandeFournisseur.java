package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private long IdCommandeFournisseur;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateCommande;
	
	@ManyToOne
	@JoinColumn(name = "IdFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy="commandeFournisseur")
	private List<LigneCommandeFournisseur> commandeFournisseurs;

	public long getIdCommandeFournisseur() {
		return IdCommandeFournisseur;
	}

	public void setIdCommandeFournisseur(long id) {
		IdCommandeFournisseur = id;
	}

	public Date getDateCommande() {
		return DateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		DateCommande = dateCommande;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getCommandeFournisseurs() {
		return commandeFournisseurs;
	}

	public void setCommandeFournisseurs(List<LigneCommandeFournisseur> commandeFournisseurs) {
		this.commandeFournisseurs = commandeFournisseurs;
	}
	
	
}
