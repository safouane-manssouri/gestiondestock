package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable{
	
	@Id
	@GeneratedValue
	private long IdCategory;
	private String Code;
	private String Designation;
	
	@OneToMany(mappedBy="Category")
	private List<Article> articles;
	
	public Category() {
	}

	public long getIdCategory() {
		return IdCategory;
	}

	public void setIdCategory(long id) {
		IdCategory = id;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
	
}
