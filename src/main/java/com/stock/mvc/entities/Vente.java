package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable{
	
	@Id
	@GeneratedValue
	private long IdVente;
	
	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateVente;
	
	@OneToMany(mappedBy="Vente")
	private List<LigneVente> ligneVentes;

	public long getIdVente() {
		return IdVente;
	}

	public void setIdVente(long id) {
		IdVente = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateVente() {
		return DateVente;
	}

	public void setDateVente(Date dateVente) {
		DateVente = dateVente;
	}

	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}

	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}
	
	
}
