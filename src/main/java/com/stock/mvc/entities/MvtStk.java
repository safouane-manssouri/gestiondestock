package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStk implements Serializable{
	
	@Id
	@GeneratedValue
	private long IdMvStock;
	
	public static final int ENTREE =1;
	public static final int SORTIE =2;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateMv;
	
	private BigDecimal Quantite;
	
	private int TypMv;
	
	@ManyToOne
	@JoinColumn(name="IdArticle")
	private Article article;

	public long getIdMvStock() {
		return IdMvStock;
	}

	public void setIdMvStock(long id) {
		IdMvStock = id;
	}

	public Date getDateMv() {
		return DateMv;
	}

	public void setDateMv(Date dateMv) {
		DateMv = dateMv;
	}

	public BigDecimal getQuantite() {
		return Quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		Quantite = quantite;
	}

	public int getTypMv() {
		return TypMv;
	}

	public void setTypMv(int typMv) {
		TypMv = typMv;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
