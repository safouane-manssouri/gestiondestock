package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private long IdLigneCommandeFournisseur;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "IdCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;
		
	public long getIdLigneCommandeFournisseur() {
		return IdLigneCommandeFournisseur;
	}

	public void setIdLigneCommandeFournisseur(long id) {
		IdLigneCommandeFournisseur = id;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	
	
}
